import tabJoursEnOrdre from './Utilitaire/gestionTemps.js';

//console.log("DEPUIS MAIN JS:" + tabJoursEnOrdre);

const APIkey ='4bbd1ead7be7c2219fdb7c52dc6898c8';
let ResultApi;
const Localisation = document.querySelector('p.localisation');
const temperature = document.querySelector('p.temperature');
const logoTemp = document.querySelector('.logo-meteo');
const hourPrevious= document.querySelectorAll('.heure-nom-prevision');
const hourDataPrevious= document.querySelectorAll('.heure-prevision-valeur');
const joursDiv = document.querySelectorAll('.jour-nom-pevision');

const tempJoursDiv = document.querySelectorAll('.jour-prevision-valeur');


//on recuper la geolocalisation 
if (navigator.geolocation){
    navigator.geolocation.getCurrentPosition (position=> {
        let latitude = position.coords.latitude;
        let longitude =position.coords.longitude;
        AppelAPI(latitude,longitude);
      },()=> {
          alert("la localisation n'est pas activé veuillez vérifier les parametre google")
       
      })
      
}
function AppelAPI(latitude,longitude){
    
    fetch(`https://api.openweathermap.org/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=minutely&units=metric&lang=fr&appid=${APIkey}`)
    .then((reponse)=>{
        if(reponse.ok){
            return reponse.json();
        }
   
    })
    .then((data)=>{
      console.log(data);
      //Valeur prinscipale  à afficher 
      Localisation.innerText=data.timezone;
      temperature.innerText=`${Math.trunc(data.current.temp)}°`;
      hourPrevious.innerText="15°";
      //Valeur à afficher tout les 3h
      let heureNow = new Date().getHours() ;
      for (let i=0;i < hourPrevious.length;i++){
          let hourIncrement = heureNow + i * 3;
          if(hourIncrement>24){
            hourPrevious[i].innerText=`${hourIncrement-24 } h`;

          }
          else if(hourIncrement===24){
            hourPrevious[i].innerText="00 h";
          }
          else {
          hourPrevious[i].innerText=`${hourIncrement} h`;
        }
      }
      for(let j=0;j <hourDataPrevious.length;j++){
          hourDataPrevious[j].innerText=`${Math.trunc(data.hourly[j * 3].temp)}°`
      }
      for(let k = 0; k < tabJoursEnOrdre.length; k++) {
        joursDiv[k].innerText = tabJoursEnOrdre[k].slice(0,3);
    }


    // Temps par jour
    for(let m = 0; m < 7; m++){
        tempJoursDiv[m].innerText = `${Math.trunc(data.daily[m + 1].temp.day)}°`
    }

    // Icone dynamique 
     if(heureNow >= 6 && heureNow< 21) {
        logoTemp.setAttribute("src",`./ressources/nuit/${data.current.weather[0].icon}.svg`);
    }
     else  {
        logoTemp.setAttribute("src",`./ressources/nuit/${data.current.weather[0].icon}.svg`);
    }
    })
    .catch((err)=>{
        alert('une ereur est survenue')
        console.log(err);
    });
}

